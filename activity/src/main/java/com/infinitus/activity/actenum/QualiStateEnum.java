package com.infinitus.activity.actenum;

/**
 * @desc: 资格状态
 */
public enum QualiStateEnum {
    AVAILABLE(1), UNAVAILABLE(2),CHARGEBACK(3);
    //环境：1-可用，2-不可用，3-退单状态
    private final int state;

    QualiStateEnum(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }
}
