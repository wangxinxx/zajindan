package com.infinitus.activity.actenum;

/**
 * @desc:
 */
public enum ValiLotteryEnum {
    SUCCESS(1), NO_QUALIFICATION(2),TIMES_END(3);
    //环境：1-成功，2-没有抽奖资格，3-抽奖次数已完
    private final int state;

    ValiLotteryEnum(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }
}
