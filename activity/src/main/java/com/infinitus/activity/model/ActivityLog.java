package com.infinitus.activity.model;

import java.io.Serializable;

/**
 * @create: 2019/1/9
 * @desc:
 */
public class ActivityLog implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    //Id
    private Long id;
    //渠道id
    private Integer cId;
    //ip
    private String ip;
    //当前请求的 User-Agent: 头部的内容
    private String agent;
    //类型 [1:浏览,2:分享
    private Integer type;
    //添加时间戳
    private Long addTime;
    //添加日期[20160907]
    private Integer day;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getcId() {
        return cId;
    }
    public void setcId(Integer cId) {
        this.cId = cId;
    }
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getAgent() {
        return agent;
    }
    public void setAgent(String agent) {
        this.agent = agent;
    }
    public Integer getType() {
        return type;
    }
    public void setType(Integer type) {
        this.type = type;
    }
    public Long getAddTime() {
        return addTime;
    }
    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }
    public Integer getDay() {
        return day;
    }
    public void setDay(Integer day) {
        this.day = day;
    }

}