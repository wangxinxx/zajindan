package com.infinitus.activity.pojo.VO;

import java.io.Serializable;
import java.util.Date;

/**
 *  * <p>
 * <pre>
 * cardNo	String	必填	卡号
 * source_system	String	必填	积分来源系统	HP:海培系统
 * integral_type	Int	必填	积分类型 	5：纷享币
 * remark	String	必填	备注	海外培训抽奖
 * interactivate_date	Date	必填	互动时间	系统时间，日期格式:月/日/年 时：分：秒
 * pointValue	int	必填	积分值
 * activte_type	String	必填	活动类型	HP01 海外培训抽奖活动
 * </pre>
 * </p>
 * <p>
 */
public class AddPointsVO implements Serializable {

    private static final long serialVersionUID =  7932565225343776157L;
    private String pointValue;// >100</pointValue>
    private String remark = "海培抽奖"; //>海培抽奖</remark>
    private String activeType = "HP01"; //>HP01</active_type>
    private Date interactivateDate; //>01/04/2019 10:16:30</interactivate_date>
    private String sourceSystem = "HP"; //>HP</source_system>
    private String cardNo; // >151562866</cardNo>
    private String integralType = "5"; // 5</integral_type>

    public String getPointValue() {
        return pointValue;
    }

    public void setPointValue(String pointValue) {
        this.pointValue = pointValue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getActiveType() {
        return activeType;
    }

    public void setActiveType(String activeType) {
        this.activeType = activeType;
    }

    public Date getInteractivateDate() {
        return interactivateDate;
    }

    public void setInteractivateDate(Date interactivateDate) {
        this.interactivateDate = interactivateDate;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getIntegralType() {
        return integralType;
    }

    public void setIntegralType(String integralType) {
        this.integralType = integralType;
    }
}