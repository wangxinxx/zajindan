package com.infinitus.activity.pojo.VO;

/**
 * @author: luzhenjie
 * @create: 2019/4/1
 * @desc:
 */
public class ReturnVo {

    private boolean success;
    private String httpStatus;
    private String errorCode;
    private String errorMessage;
    private String exceptionCode;
    private String exceptionMessage;
    private String exceptionStack;
    private ApiVo apiVo;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(String httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    public void setExceptionCode(String exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public String getExceptionStack() {
        return exceptionStack;
    }

    public void setExceptionStack(String exceptionStack) {
        this.exceptionStack = exceptionStack;
    }

    public ApiVo getApiVo() {
        return apiVo;
    }

    public void setApiVo(ApiVo apiVo) {
        this.apiVo = apiVo;
    }
}