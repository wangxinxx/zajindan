package com.infinitus.activity.pojo.VO;

import java.util.List;

/**
 * @create: 2019/1/9
 * @desc:
 */
public class ActivityConfigVO {

    private String activityStartTime;

    private String activityEndTime;
    private Integer lotteryTimes;
    private String lotteryStartDate;
    private String lotteryEndDate;

    private String activityTime ;

    private String lotteryTime;

    private String ruleType;//1-抽奖资格抽奖 0-非抽奖资格抽奖

    private List<AwardsConfigVO> awardsConfigList;

    public List<AwardsConfigVO> getAwardsConfigList() {
        return awardsConfigList;
    }

    public String getActivityStartTime() {
        return activityStartTime;
    }

    public void setActivityStartTime(String activityStartTime) {
        this.activityStartTime = activityStartTime;
    }

    public String getActivityEndTime() {
        return activityEndTime;
    }

    public void setActivityEndTime(String activityEndTime) {
        this.activityEndTime = activityEndTime;
    }

    public Integer getLotteryTimes() {
        return lotteryTimes;
    }

    public void setLotteryTimes(Integer lotteryTimes) {
        this.lotteryTimes = lotteryTimes;
    }

    public String getLotteryStartDate() {
        return lotteryStartDate;
    }

    public void setLotteryStartDate(String lotteryStartDate) {
        this.lotteryStartDate = lotteryStartDate;
    }

    public String getLotteryEndDate() {
        return lotteryEndDate;
    }

    public void setLotteryEndDate(String lotteryEndDate) {
        this.lotteryEndDate = lotteryEndDate;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }

    public String getLotteryTime() {
        return lotteryTime;
    }

    public void setLotteryTime(String lotteryTime) {
        this.lotteryTime = lotteryTime;
    }

    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public void setAwardsConfigList(List<AwardsConfigVO> awardsConfigList) {
        this.awardsConfigList = awardsConfigList;
    }
}