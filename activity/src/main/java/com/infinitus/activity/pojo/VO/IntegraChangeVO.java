package com.infinitus.activity.pojo.VO;

/**
 * @create: 2019/1/9
 * @desc: 纷享币兑换接口vo
 */
public class IntegraChangeVO {
    /**
     * 会员卡号
     */
    private String cardNo;
    /**
     * 如："ACTS"
     */
    private String sourceSystem;
    /**
     * 如："我的积分抽奖"
     */
    private String remark;
    /**
     * 该属性固定的：03
     */
    private String pointOperType;
    /**
     * 该属性固定的："兑换"
     */
    private String reason;
    /**
     * 分享币数量
     */
    private String amount;
    /**
     * 该属性固定的："05"
     */
    private String integralType;
    /**
     * 该属性固定的："1"
     */
    private String operType;
    /**
     * 没有使用到
     */
    private String orderNum;

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPointOperType() {
        return pointOperType;
    }

    public void setPointOperType(String pointOperType) {
        this.pointOperType = pointOperType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getIntegralType() {
        return integralType;
    }

    public void setIntegralType(String integralType) {
        this.integralType = integralType;
    }

    public String getOperType() {
        return operType;
    }

    public void setOperType(String operType) {
        this.operType = operType;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }
}