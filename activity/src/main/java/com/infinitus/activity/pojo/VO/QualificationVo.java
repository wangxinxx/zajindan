package com.infinitus.activity.pojo.VO;


/**
 * @author: luzhenjie
 * @create: 2019/3/11
 * @desc:
 */

public class QualificationVo {

    //卡号
    private String cardNo;

    //活动编号
    private String promCode;

    //资格序列号
    private String qualiSeqNo;

    //资格状态
    private int qualiStatus;


    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getPromCode() {
        return promCode;
    }

    public void setPromCode(String promCode) {
        this.promCode = promCode;
    }

    public String getQualiSeqNo() {
        return qualiSeqNo;
    }

    public void setQualiSeqNo(String qualiSeqNo) {
        this.qualiSeqNo = qualiSeqNo;
    }

    public int getQualiStatus() {
        return qualiStatus;
    }

    public void setQualiStatus(int qualiStatus) {
        this.qualiStatus = qualiStatus;
    }
}