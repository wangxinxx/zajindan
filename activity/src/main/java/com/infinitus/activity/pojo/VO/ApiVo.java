package com.infinitus.activity.pojo.VO;

/**
 * @author: luzhenjie
 * @create: 2019/4/1
 * @desc:
 */
public class ApiVo {

    private boolean bizData;
    private String bizMsgCode;
    private String bizMsg;

    public boolean isBizData() {
        return bizData;
    }

    public void setBizData(boolean bizData) {
        this.bizData = bizData;
    }

    public String getBizMsgCode() {
        return bizMsgCode;
    }

    public void setBizMsgCode(String bizMsgCode) {
        this.bizMsgCode = bizMsgCode;
    }

    public String getBizMsg() {
        return bizMsg;
    }

    public void setBizMsg(String bizMsg) {
        this.bizMsg = bizMsg;
    }
}