package com.infinitus.activity.pojo.DO;

import java.util.Date;

/**
 * @create: 2019/1/9
 * @desc:
 */
public class AuldrawWinnerDo {

    private Long id;

    private String dealerNo;

    private String activityCode;

    private String awardsId;

    private String porcCode;

    private Date createTime;

    public String getPorcCode() {
        return porcCode;
    }

    public void setPorcCode(String porcCode) {
        this.porcCode = porcCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealerNo() {
        return dealerNo;
    }

    public void setDealerNo(String dealerNo) {
        this.dealerNo = dealerNo;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getAwardsId() {
        return awardsId;
    }

    public void setAwardsId(String awardsId) {
        this.awardsId = awardsId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}