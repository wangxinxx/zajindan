package com.infinitus.activity.pojo.DTO;

/**
 * @create: 2019/1/9
 * @desc:
 */
public class FenXiangHuiPushReturnDto {

    private String rCode;
    private String rMsg;
    public String getrCode() {
        return rCode;
    }
    public void setrCode(String rCode) {
        this.rCode = rCode;
    }
    public String getrMsg() {
        return rMsg;
    }
    public void setrMsg(String rMsg) {
        this.rMsg = rMsg;
    }
}