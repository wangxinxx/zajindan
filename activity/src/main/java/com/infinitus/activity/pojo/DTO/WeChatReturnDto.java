package com.infinitus.activity.pojo.DTO;

/**
 * @author: luzhenjie
 * @create: 2019/3/11
 * @desc:
 */
public class WeChatReturnDto {

    private String result;

    private String msg;

    private Object data;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}