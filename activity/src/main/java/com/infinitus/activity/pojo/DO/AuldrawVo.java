package com.infinitus.activity.pojo.DO;


import java.util.Date;

/**
 * @author: luzhenjie
 * @create: 2019/3/15
 * @desc:  中奖信息和收货地址Vo
 */

public class AuldrawVo {
    //卡号
    private String dealerNo;
    //奖品名称
    private String awardsName;
    //奖品代码id
    private String giftCode;
    //是否中奖 1-中奖  0-没有中奖
    private Integer winningPrize;
    //资格序列号
    private String qualiSeqNo;
    //活动编码
    private String activityCode;
    //奖品数量
    private Integer giftQty;
    //收货人名称
    private String receiverName;
    //收货人电话
    private String receiverPhone;
    //省份编码
    private String provinceCode;
    //城市编码
    private String cityCode;
    //区县编码
    private String townCode;
    //详细地址
    private String detailAddress;
    //创建时间
    private Date createTime;

    public Integer getGiftQty() {
        return giftQty;
    }

    public void setGiftQty(Integer giftQty) {
        this.giftQty = giftQty;
    }

    public String getAwardsName() {
        return awardsName;
    }

    public void setAwardsName(String awardsName) {
        this.awardsName = awardsName;
    }

    public String getGiftCode() {
        return giftCode;
    }

    public void setGiftCode(String giftCode) {
        this.giftCode = giftCode;
    }

    public Integer getWinningPrize() {
        return winningPrize;
    }

    public void setWinningPrize(Integer winningPrize) {
        this.winningPrize = winningPrize;
    }

    public String getDealerNo() {
        return dealerNo;
    }

    public void setDealerNo(String dealerNo) {
        this.dealerNo = dealerNo;
    }

    public String getQualiSeqNo() {
        return qualiSeqNo;
    }

    public void setQualiSeqNo(String qualiSeqNo) {
        this.qualiSeqNo = qualiSeqNo;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getTownCode() {
        return townCode;
    }

    public void setTownCode(String townCode) {
        this.townCode = townCode;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}