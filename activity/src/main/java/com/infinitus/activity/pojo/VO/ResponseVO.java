package com.infinitus.activity.pojo.VO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @create: 2019/1/9
 * @desc:
 */
@JsonInclude(Include.NON_NULL)
public class ResponseVO<T> {
    private boolean success=true;

    private int code = 0;

    private T content;

    public int getCode() {
        return code;
    }

    public ResponseVO<T> setCode(int code) {
        this.code = code;
        return this;
    }

    public T getContent() {
        return content;
    }

    public ResponseVO<T> setContent(T content) {
        this.content = content;
        return this;
    }


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}