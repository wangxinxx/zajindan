package com.infinitus.activity.pojo.VO;


import java.math.BigDecimal;

/**
 * @create: 2019/1/9
 * @desc:
 */

public class AwardsConfigVO {

    private String awardsName;
    private String awardsLevel;
    private String awardsCode;
    //促销系统奖品编码
    private String giftCode;
    private Integer awardsCounts;  //前端配置数量
    private Integer giftQty;       //奖品可抽取次数
    private BigDecimal awardsRate;
    private String prizeType;

    private String qualiSeqNo;  //资格序列号:返回前端用于区别每一个中奖纪录

    public Integer getGiftQty() {
        return giftQty;
    }

    public void setGiftQty(Integer giftQty) {
        this.giftQty = giftQty;
    }

    public String getAwardsName() {
        return awardsName;
    }

    public void setAwardsName(String awardsName) {
        this.awardsName = awardsName;
    }

    public String getAwardsLevel() {
        return awardsLevel;
    }

    public void setAwardsLevel(String awardsLevel) {
        this.awardsLevel = awardsLevel;
    }

    public String getAwardsCode() {
        return awardsCode;
    }

    public void setAwardsCode(String awardsCode) {
        this.awardsCode = awardsCode;
    }

    public String getGiftCode() {
        return giftCode;
    }

    public void setGiftCode(String giftCode) {
        this.giftCode = giftCode;
    }

    public Integer getAwardsCounts() {
        return awardsCounts;
    }

    public void setAwardsCounts(Integer awardsCounts) {
        this.awardsCounts = awardsCounts;
    }

    public BigDecimal getAwardsRate() {
        return awardsRate;
    }

    public void setAwardsRate(BigDecimal awardsRate) {
        this.awardsRate = awardsRate;
    }

    public String getPrizeType() {
        return prizeType;
    }

    public void setPrizeType(String prizeType) {
        this.prizeType = prizeType;
    }

    public String getQualiSeqNo() {
        return qualiSeqNo;
    }

    public void setQualiSeqNo(String qualiSeqNo) {
        this.qualiSeqNo = qualiSeqNo;
    }
}