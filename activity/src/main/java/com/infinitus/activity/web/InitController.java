package com.infinitus.activity.web;

import com.infinitus.activity.pojo.DO.AuldrawWinnerDo;
import com.infinitus.activity.pojo.VO.ResponseVO;
import com.infinitus.activity.util.AwardsUtils;
import com.infinitus.activity.util.KeyGenerator;
import com.infinitus.activity.util.MD5Util;
import com.infinitus.activity.util.UrlEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @create: 2019/1/9
 * @desc:
 */
@Controller
public class InitController {
    private Logger logger = LoggerFactory.getLogger(InitController.class);

    @Resource
    private RedisTemplate<String, AuldrawWinnerDo> redisTemplate;

    @Autowired
    private RedisTemplate<String, Object> redisTemplates;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //@Autowired
    //private AwardsUtils awardsUtils;



    /**
     * 初始化奖项数量
     *
     * @param identifier
     * @return
     */
   /* @RequestMapping(path = "/initAwards/{identifier}/{timeStamp}", method = RequestMethod.GET)
    public ResponseEntity<?> initAwards(@PathVariable String identifier, @PathVariable long timeStamp) {

        Boolean exist = stringRedisTemplate.hasKey("initAwards");
        if (exist) {
            return ResponseEntity.ok(new ResponseVO<>().setCode(40001).setContent("请勿重复刷新, 3分钟后再试.."));
        }

        int currentTime = (int) (System.currentTimeMillis()/1000);
        if (currentTime - timeStamp > 180) {
            return ResponseEntity.ok(new ResponseVO<>().setCode(40003).setContent("请重新刷新签名.."));
        }


        //
        String key = "Guess I lose-" + timeStamp;

        // md5加密
        try {
            key = MD5Util.string2MD5(key);
        } catch (Exception e) {
            logger.error("MD5加密失败 > " + e.getMessage());
        }

        if (!key.equalsIgnoreCase(identifier)) {
            return ResponseEntity.ok(new ResponseVO<>().setCode(40002).setContent("密钥不匹配.."));
        }

        // 奖项
        String awards1 = awardsUtils.getPrize1();
        String awards2 = awardsUtils.getPrize2();
        String awards3 = awardsUtils.getPrize3();
        String awards4 = awardsUtils.getPrize4();
        String awards5 = awardsUtils.getPrize5();
        String awards6 = awardsUtils.getPrize6();

        // 奖品数量
        int num1 = awardsUtils.getNum1();
        int num2 = awardsUtils.getNum2();
        int num3 = awardsUtils.getNum3();
        int num4 = awardsUtils.getNum4();
        int num5 = awardsUtils.getNum5();
        int num6 = awardsUtils.getNum6();

        // 初始化数据到redis
        ValueOperations<String, String> opsForValue = stringRedisTemplate.opsForValue();
        opsForValue.set(KeyGenerator.getStringAwards(awards1), String.valueOf(num1));
        opsForValue.set(KeyGenerator.getStringAwards(awards2), String.valueOf(num2));
        opsForValue.set(KeyGenerator.getStringAwards(awards3), String.valueOf(num3));
        opsForValue.set(KeyGenerator.getStringAwards(awards4), String.valueOf(num4));
        opsForValue.set(KeyGenerator.getStringAwards(awards5), String.valueOf(num5));
        opsForValue.set(KeyGenerator.getStringAwards(awards6), String.valueOf(num6));

        // 设置防重复初始化奖项数量标识
        stringRedisTemplate.opsForValue().set("initAwards", "exist", 180, TimeUnit.SECONDS);

        // 拼接返回结果
        Map<String, Integer> map = new LinkedHashMap<>();
        map.put(awards1, num1);
        map.put(awards2, num2);
        map.put(awards3, num3);
        map.put(awards4, num4);
        map.put(awards5, num5);
        map.put(awards6, num6);

        return ResponseEntity.ok(new ResponseVO<>().setContent(map));
    }


    *//**
     * 重新加载概率, 中奖范围
     *
     * @param identifier
     * @param timeStamp
     * @return
     *//*
    @RequestMapping(path = "/reloadProbability/{identifier}/{timeStamp}", method = RequestMethod.POST)
    public ResponseEntity<?> reloadProbability(
            @PathVariable String identifier,
            @PathVariable long timeStamp,
            AwardsUtils au) {

        Boolean exist = stringRedisTemplate.hasKey("reloadProbability");
        if (exist) {
            return ResponseEntity.ok(new ResponseVO<>().setCode(40001).setContent("请勿重复刷新, 3分钟后再试.."));
        }

        int currentTime = (int) (System.currentTimeMillis() / 1000);
        if (currentTime - timeStamp > 180) {
            return ResponseEntity.ok(new ResponseVO<>().setCode(40003).setContent("请重新刷新签名.."));
        }

        String key = "Guess I lose-" + timeStamp;

        // md5加密
        try {
            key = MD5Util.string2MD5(key);
        } catch (Exception e) {
            logger.error("MD5加密失败 > " + e.getMessage());
        }

        if (!key.equalsIgnoreCase(identifier)) {
            return ResponseEntity.ok(new ResponseVO<>().setCode(40002).setContent("密钥不匹配.."));
        }

        // 设置防重复初始化奖项数量标识
        stringRedisTemplate.opsForValue().set("reloadProbability", "reloadProbability", 180, TimeUnit.SECONDS);

        // 重新设置概率
        awardsUtils.setProportion1(au.getProportion1());
        awardsUtils.setProportion2(au.getProportion2());
        awardsUtils.setProportion3(au.getProportion3());
        awardsUtils.setProportion4(au.getProportion4());
        awardsUtils.setProportion5(au.getProportion5());
        awardsUtils.setProportion6(au.getProportion6());

        // 重新中奖区间
        awardsUtils.reload();

        // 新的区间
        String range =
                awardsUtils.getPrize1() + " : " + awardsUtils.getRange1() + ", " +
                        awardsUtils.getPrize2() + " : " + awardsUtils.getRange2() + ", " +
                        awardsUtils.getPrize3() + " : " + awardsUtils.getRange3() + ", " +
                        awardsUtils.getPrize4() + " : " + awardsUtils.getRange4() + ", " +
                        awardsUtils.getPrize5() + " : " + awardsUtils.getRange5() + ", " +
                        awardsUtils.getPrize6() + " : " + awardsUtils.getRange6();
        // 新的概率
        String proportion =
                awardsUtils.getPrize1() + " : " + awardsUtils.getProportion1() + ", " +
                        awardsUtils.getPrize2() + " : " + awardsUtils.getProportion2() + ", " +
                        awardsUtils.getPrize3() + " : " + awardsUtils.getProportion3() + ", " +
                        awardsUtils.getPrize4() + " : " + awardsUtils.getProportion4() + ", " +
                        awardsUtils.getPrize5() + " : " + awardsUtils.getProportion5() + ", " +
                        awardsUtils.getPrize6() + " : " + awardsUtils.getProportion6();

        // 拼接返回结果
        Map<String, String> map = new LinkedHashMap<>();
        map.put("区间", range);
        map.put("概率", proportion);

        return ResponseEntity.ok(new ResponseVO<>().setContent(map));
    }

    public static void main(String[] args) {
    }

    @RequestMapping(value = "/getToken",method = RequestMethod.GET)
    @ResponseBody
    public String getDealerNo(){
        lotteryOperation1();

        int dealerNo = (int)((Math.random()*9+1)*100000000);

        return UrlEncryptor.encrypt(String.valueOf(dealerNo));
    }

    private String lotteryOperation1() {
        redisTemplates.delete("rewards");
        System.out.println(redisTemplates.opsForList().size("rewards"));
        redisTemplates.opsForList().rightPush("rewards",awardsUtils.getPrize1());
        redisTemplates.opsForList().rightPush("rewards",awardsUtils.getPrize2());
        redisTemplates.opsForList().rightPush("rewards",awardsUtils.getPrize3());
        redisTemplates.opsForList().rightPush("rewards",awardsUtils.getPrize4());
        redisTemplates.opsForList().rightPush("rewards",awardsUtils.getPrize5());
        redisTemplates.opsForList().rightPush("rewards",awardsUtils.getPrize6());
        System.out.println(redisTemplates.opsForList().range("rewards",0,-1));
        System.out.println(redisTemplates.opsForList().index("rewards",2));
        redisTemplates.opsForList().remove("rewards",1, awardsUtils.getPrize3());
        System.out.println(redisTemplates.opsForList().range("rewards",0,-1));
        System.out.println(redisTemplates.opsForList().index("rewards",2));
        System.out.println(redisTemplates.opsForList().size("rewards"));
        return "";
    }*/
}