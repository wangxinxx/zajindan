package com.infinitus.activity.web;

import com.infinitus.activity.util.HttpClientUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @create: 2019/1/9
 * @desc:
 */
@Component
@RequestMapping(value = "/http")
public class HttpController {
    @Value("${ydapi.env}")
    public String ydapiEnv;

    /**
     * 根据卡号获取用户没有填配送地址的接口
     * @param dealerNo
     * @return
     */
    @RequestMapping(value = "/tranGet")
    @ResponseBody
    public String httpTranGet(String url,String env) throws Exception{
        String resultStr = null;
        if(url==null||env==null){
            return "url 和 env 不能为空  env 为test or prd !";
        }
        resultStr = HttpClientUtils.doGet(ydapiEnv+url, null);
        return resultStr;
    }

    @RequestMapping(value = "/tranPost")
    @ResponseBody
    public String httpTranPost(String url,String env) throws Exception{
        String resultStr = null;
        if(url==null||env==null){
            return "url 和 env 不能为空  env 为test or prd !";
        }
        Map<String,String> params=new HashMap<String,String>();
        resultStr = HttpClientUtils.doPost(ydapiEnv+url, params);
        return resultStr;
    }
}