package com.infinitus.activity.web;

import com.infinitus.activity.model.ActivityLog;
import com.infinitus.activity.util.ActivityLogUtil;
import com.infinitus.activity.util.FileUtils;
import com.jfinal.kit.PathKit;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.util.Calendar;

/**
 * @create: 2019/1/9
 * @desc:
 */
@Controller
@RequestMapping(value = "/activity")
public class ActivityLogController {
    @RequestMapping(value = "/test")
    @ResponseBody
    public Object test(HttpServletRequest request, String name) {
        System.out.println("name============="+name);
        return "我是中文";
    }
    /**
     *
     * @Title: saveLog
     * @Description: 保存日志
     * @author    add   by zhangxh 2017年1月4日 下午1:40:12
     * @param request
     * @param cId
     * @param type
     * @return
     * @throws
     *
     */
    @RequestMapping(value = "/saveLog")
    @ResponseBody
    public Object saveLog(HttpServletRequest request,Integer cId,Integer type) {
        //System.out.println("name============="+name);
        String agent = request.getHeader("user-agent");
        ActivityLog log = new ActivityLog();
        String ip = getIpAddr(request);
        log.setAgent(agent);
        log.setcId(cId);
        log.setType(type);
        log.setIp(ip);
        ActivityLogUtil util = new ActivityLogUtil();
        String result = util.saveLog(log);
        return result;
    }

    /**
     *
     * @Title: getIpAddr
     * @Description: 获取客户端IP
     * @author    add   by zhangxh 2017年1月4日 上午9:26:04
     * @param request
     * @return
     * @throws
     *
     */
    private String getIpAddr(HttpServletRequest request) {
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")) {
                // 根据网卡取本机配置的IP
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                ipAddress = inet.getHostAddress();
            }
        }
        // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (ipAddress != null && ipAddress.length() > 15) { // "***.***.***.***".length()
            // = 15
            if (ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }
        }
        return ipAddress;
    }

    /**
     *
     * @Title: readLog
     * @Description: 读取日志信息
     * @author    add   by zhangxh 2017年3月10日 上午9:58:35
     * @param request
     * @param type
     * @return
     * @throws
     *
     */
    @RequestMapping(value = "/readLog")
    @ResponseBody
    public Object readLog(HttpServletRequest request,Integer type) {
        String path = Paths.get(PathKit.getWebRootPath(),"logstxt").toString();
        String fileName = "";
        if( type == 2 ){
            Calendar c = Calendar.getInstance();// 可以对每个时间域单独修改
            int date = c.get(Calendar.DATE);
            fileName = "ecs" + date + ".txt";
        }else{
            fileName = "ecs.txt";
        }
        //明细文件
        File file = new File(path +File.separator+ fileName);
        StringBuilder content = FileUtils.readContent(file);
        return content.toString();
    }
}