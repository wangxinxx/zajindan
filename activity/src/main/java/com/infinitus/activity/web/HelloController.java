package com.infinitus.activity.web;


import com.infinitus.activity.model.KeyValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: luzhenjie
 * @create: 2019/1/9
 * @desc:
 */
@Controller
@RequestMapping(value="/springboot")
public class HelloController {

    private static final Logger logger = LoggerFactory.getLogger(HelloController.class);
    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    @ResponseBody
    public String say() {
        logger.info("打印日志！！！！！！！！！！！！");
        return "This is SpringBoot!";
    }

    @RequestMapping(value = "/index",method = RequestMethod.GET)
    public String toIndex() {
        return "index";
    }



    public static void main(String[] args) {
        HashMap<String, KeyValue> map = new HashMap<>();


        map.put("1",new KeyValue("key1","value1"));
        map.put("2",new KeyValue("key2","value2"));
        map.put("3",new KeyValue("key3","value3"));
        map.put("4",new KeyValue("key4","value4"));
        map.put("5",new KeyValue("key5","value5"));


        for(Map.Entry<String,KeyValue> entry : map.entrySet()) {
            System.out.println(entry.getValue());
        }
    }
}