package com.infinitus.activity.web;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.infinitus.activity.common.WebConstant;
import com.infinitus.activity.redis.RedisService;
/**
 * @create: 2019/1/9
 * @desc:
 */
@Component
@RequestMapping(value = "/redis")
public class RedisController {
    @Autowired
    RedisService redisService;


    @RequestMapping(value = "/get")
    @ResponseBody
    public Object get(@RequestParam(required=true)String key) {
        Map<String,Object> result=new HashMap<String, Object>();
        try {
            Object obj=redisService.get(key);
            result.put(WebConstant.RESULT_FILED, WebConstant.SUCCESS);
            result.put(WebConstant.RESULT_DATA, obj);
        } catch (Exception e) {
            result.put(WebConstant.RESULT_FILED, WebConstant.ERROR);
            result.put(WebConstant.RESULT_MESSAGE, e.toString());
        }
        return result;
    }

    @RequestMapping(value = "/set")
    @ResponseBody
    public Object set(@RequestParam(required=true)String key,@RequestParam(required=true)String value) {
        Map<String,Object> result=new HashMap<String, Object>();
        try {
            redisService.set(key,value);
            result.put(WebConstant.RESULT_FILED, WebConstant.SUCCESS);
        } catch (Exception e) {
            result.put(WebConstant.RESULT_FILED, WebConstant.ERROR);
            result.put(WebConstant.RESULT_MESSAGE, e.toString());
        }
        return result;
    }

    @RequestMapping(value = "/del")
    @ResponseBody
    public Object del(@RequestParam(required=true)String key) {
        Map<String,Object> result=new HashMap<String, Object>();
        try {
            redisService.del(key);
            result.put(WebConstant.RESULT_FILED, WebConstant.SUCCESS);
        } catch (Exception e) {
            result.put(WebConstant.RESULT_FILED, WebConstant.ERROR);
            result.put(WebConstant.RESULT_MESSAGE, e.toString());
        }
        return result;
    }
}