package com.infinitus.activity.common;

/**
 * @create: 2019/1/9
 * @desc:
 */
public class RedisKey {
    private static final String DFM_jsConfig="DFM_jsConfig";

    private static final String DFM_SMSVerifyCode="DFM_SMSVerifyCode";
    private static final String DFM_loginErrorCount="DFM_loginErrorCount";
    private static final String DFM_groupBranchPlayList="DFM_groupBranchPlayList";
    public static final String DFM_player="DFM_player";
    public static final String DFM_random10dplayer="DFM_random10dplayer";
    public static final String DFM_wechatConfig="DFM_wechatConfig";
    public static final String WECHAT_accessToken="WECHAT_accessToken";
    public static final String WECHAT_jsTicker="WECHAT_jsTicker";

    public static String buildDFM_SmsVerify(String phoneNo){
        return DFM_SMSVerifyCode+"_"+phoneNo;
    }
    public static String buildDFM_jsConfig(String platform){
        return DFM_jsConfig+"_"+platform;
    }
    public static String buildDFM_loginErrorCount(String dealerNo){
        return DFM_loginErrorCount+"_"+dealerNo;
    }
    public static String buildDFM_groupBranchPlayList(String groupType,String saleBranchNo){
        return DFM_groupBranchPlayList+"_"+groupType+"_"+saleBranchNo;
    }
}