package com.infinitus.activity.common;

/**
 * @类功能说明：  web常量类
 * @作者：Winsons
 * @创建时间：2015-7-7
 * @版本：V1.0
 */
public class WebConstant {
    /*返回对象字段*/
    public static final String RESULT_FILED = "result";

    /*返回对象数据*/
    public static final String RESULT_DATA = "data";

    /*返回信息字符串*/
    public static final String RESULT_MESSAGE = "msg";

    /*请求参数字符串*/
    public static final String REQUEST_PARAM = "params";

    /*错误内容*/
    public static final String ERROR_CONTENT = "error_content";

    /*失败*/
    public static final String FAIL = "400";

    /*未授权*/
    public static final String AUTH = "401";

    /*错误*/
    public static final String ERROR = "500";

    /*限流*/
    public static final String LIMIT = "503";

    /*关闭*/
    public static final String CLOSE = "900";

    /*等待*/
    public static final String WAIT = "408";

    /*成功*/
    public static final String SUCCESS = "200";

    /*申请*/
    public static final String APPLY = "300";

    /*返回信息字符串*/
    public static final String MESSAGE_OK = "ok";
}