package com.infinitus.activity.common;

/**
 * @author: luzhenjie
 * @create: 2019/3/12
 * @desc:
 */
public final class ResponseCodeConstant {

    //卡号不合法
    public static final Integer ILLEGAL_DEALERNO=40001;

    //尚未登录或已过期
    public static final Integer UNLOGIN=40002;

    //抽过奖但未中奖
    public static final Integer LOTTERY_BUT_FAIL=40003;

    //抽过奖并且中奖
    public static final Integer LOTTERY_AND_WIN=40004;

    //活动未开始
    public static final Integer ACTIVITY_UNSTART=40005;

    //活动已结束
    public static final Integer ACTIVITY_ENDED=40006;

    //抽奖次数已用完
    public static final Integer LOTTERY_TIMES_END = 40007;

    //没有抽奖资格
    public static final Integer NO_LOTTERY_QUA = 40008;
}