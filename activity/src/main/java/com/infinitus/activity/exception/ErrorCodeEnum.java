package com.infinitus.activity.exception;

/**
 * @desc:
 */
public enum ErrorCodeEnum {

    E_1001("1001","非满标状态");

    private String errorCode;     //错误代码
    private String errorMessage;  //错误信息

    ErrorCodeEnum(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
