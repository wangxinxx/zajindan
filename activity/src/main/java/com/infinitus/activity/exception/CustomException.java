package com.infinitus.activity.exception;

/**
 * @author: luzhenjie
 * @create: 2019/1/10
 * @desc:
 */
public class CustomException extends Exception {

    private static final long serialVersionUID = 1L;

    private String errorCode;     //错误代码

    //无参构造
    public CustomException() {
    }

    public CustomException(String message) {
        super(message);
    }

    //带错误代码,错误信息的有参构造
    public CustomException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public CustomException(ErrorCodeEnum errorCodeEnum) {
        super(errorCodeEnum.getErrorMessage());
        this.errorCode=errorCodeEnum.getErrorCode();
    }


    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}