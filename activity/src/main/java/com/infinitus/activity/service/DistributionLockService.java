package com.infinitus.activity.service;

import org.springframework.stereotype.Component;

/**
 * @desc:
 */
@Component
public interface DistributionLockService {
    //加锁成功 返回加锁时间
    Boolean lock(String lockKey,long time,String clientId);

    //解锁 需要更加加锁时间判断是否有权限
    Boolean unlock(String lockKey,String clientId);
}
