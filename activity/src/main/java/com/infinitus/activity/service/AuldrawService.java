package com.infinitus.activity.service;

import com.infinitus.activity.pojo.DO.AuldrawAddressDo;
import com.infinitus.activity.pojo.DO.AuldrawWinnerDo;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * @desc:
 */
public interface AuldrawService {

    //根据卡号获取中奖纪录
    public List<AuldrawWinnerDo> selectAuldrawWinnerListByCardNo(String cardNo);

    //根据卡号获取已填写的收货地址
    public List<AuldrawAddressDo> selectAuldrawAddressListByCardNo(String cardNo);

    //填写收货地址
    public void createAuldrawAddress(AuldrawAddressDo auldrawAddressDo) throws Exception;



}
