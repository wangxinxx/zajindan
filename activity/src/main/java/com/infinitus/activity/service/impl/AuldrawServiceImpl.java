package com.infinitus.activity.service.impl;

import com.infinitus.activity.pojo.DO.AuldrawAddressDo;
import com.infinitus.activity.pojo.DO.AuldrawWinnerDo;
import com.infinitus.activity.service.AuldrawService;
import com.infinitus.activity.util.KeyGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: luzhenjie
 * @create: 2019/3/11
 * @desc:
 */
@Service
public class AuldrawServiceImpl implements AuldrawService{

    @Value("${deploycolId}")
    private String deploycolId;

    @Resource
    private RedisTemplate<String,AuldrawAddressDo> redisTemplate;

    /**
     * 根据卡号获取中奖纪录
     *
     * @param cardNo
     * @return
     */
    public List<AuldrawWinnerDo> selectAuldrawWinnerListByCardNo(String cardNo){
        //return auldrawWinnerMapper.selectByCardNo(cardNo);
        return null;
    }

    /**
     * 根据卡号获取已填写的收货地址
     *
     * @param cardNo
     * @return
     */
    public List<AuldrawAddressDo> selectAuldrawAddressListByCardNo(String cardNo){
        return null;
        //return auldrawAddressMapper.selectByCardNo(cardNo);
    }

    /**
     * 填写收货地址
     *
     * @param auldrawAddressDo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public void createAuldrawAddress(AuldrawAddressDo auldrawAddressDo)throws Exception{
        //将收货地址保存到redis
        try {
            String dealerNo = auldrawAddressDo.getCardNo();
            redisTemplate.opsForList().leftPush(KeyGenerator.getAwardsAddressWinningBackup(deploycolId,dealerNo), auldrawAddressDo);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }



    /**
     * 修改中奖纪录表中“是否填写了收货地址”字段的值
     * @param cardNo
     * @return
     */
    public int updateIsWriteAddressByCardNo(String cardNo) {
        return 0;
        //return auldrawWinnerMapper.updateIsWriteAddressByCardNo(cardNo);

    }
}