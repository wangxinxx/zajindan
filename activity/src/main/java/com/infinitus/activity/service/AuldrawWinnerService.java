package com.infinitus.activity.service;

import org.springframework.stereotype.Component;

/**
 * @desc:
 */
@Component
public interface AuldrawWinnerService {
    Integer countByDealerNo(String dealerNo);
}
