package com.infinitus.activity.service;

import com.infinitus.activity.pojo.DO.ConfigDo;
import org.springframework.stereotype.Component;

/**
 * @desc:
 */
@Component
public interface ConfigService {
    ConfigDo getConfig();
}
