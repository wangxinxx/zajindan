package com.infinitus.activity.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @create: 2019/1/9
 * @desc:
 */
public class ConnectionUtil {
    // 创建静态全局变量
    static Connection con;

    public static void main(String args[]){
        Connection con = getConnection();
        System.out.println("con=========="+con);
    }

    /* 获取数据库连接的函数*/
    public static Connection getConnection() {
        Connection con = null;	//创建用于连接数据库的Connection对象
        try {
            Class.forName("com.mysql.jdbc.Driver");// 加载Mysql数据驱动
            con = DriverManager.getConnection(
                    PropertiesUtil.getConfigInfo().getProperty("jdbc.url"),
                    PropertiesUtil.getConfigInfo().getProperty("jdbc.username"),
                    PropertiesUtil.getConfigInfo().getProperty("jdbc.password"));// 创建数据连接
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            String strs = sw.toString();
            System.out.println("数据库连接失败" + strs);
        }
        return con;	//返回所建立的数据库连接
    }
}