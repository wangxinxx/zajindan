package com.infinitus.activity.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @create: 2019/1/9
 * @desc:
 */
public class PropertiesUtil extends Properties{
    private static final long serialVersionUID = 1L;

    /**
     *
     * @method-function：    只加载一次
     * @modify-desc：
     * @return
     */
    public static PropertiesUtil getConfigInfo() {
        return new PropertiesUtil("config.properties");
    }

    /**
     *
     * @method-function：
     * @modify-desc：
     * @param resourceName
     * @return
     */
    public static PropertiesUtil getResource(String resourceName) {
        return new PropertiesUtil(resourceName); //现在保持每一次都加载
    }

    /**
     * 加载默认配置文件
     */
    private PropertiesUtil() {
        try {
            InputStream is = this.getClass().getClassLoader().getResourceAsStream("config.properties");
            // 把这个文件流加载到 这个类里
            load(is);
        } catch (Exception e) {
            System.out.println("读取默认配置文件异常" + e.getMessage());
        }
    }

    private PropertiesUtil(String resourceName) {
        try {
            InputStream is = this.getClass().getClassLoader().getResourceAsStream(resourceName);
            // 把这个文件流加载到 这个类里
            load(is);
            System.out.println("加载配置文件成功:" + resourceName);
        } catch (Exception e) {
            System.out.println("读取提示描述配置文件异常" + e.fillInStackTrace().getMessage());
        }
    }

    public static void main(String[] args) {
        System.out.println(PropertiesUtil.getConfigInfo().getProperty("jdbc.url"));
        System.out.println(PropertiesUtil.getResource("myconfig.properties").getProperty("endpoint.url"));
    }
}