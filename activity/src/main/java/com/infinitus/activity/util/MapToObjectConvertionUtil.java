package com.infinitus.activity.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: luzhenjie
 * @create: 2019/3/5
 * @desc:
 */
public class MapToObjectConvertionUtil {
    private final static String KEY="key";
    private final static String VALUE="value";


    public static Map josnArrayToMap(JSONArray jsonarray) {
        Map<Object,Object> map= new HashMap<Object, Object>();
        for (Object objectTemp : jsonarray) {
            JSONObject obj2= (JSONObject)objectTemp;
            map.put(obj2.get(MapToObjectConvertionUtil.KEY), obj2.get(MapToObjectConvertionUtil.VALUE));
        }
        return map;
    }
    public static <T> T mapToObject(Map<Object, Object> map, Class<T> t) throws InstantiationException, IllegalAccessException{
        if (map == null)
            return null;
        T tObj = t.newInstance();
        Field[] fields = tObj.getClass().getDeclaredFields();
        for (Field field : fields) {
            int mod = field.getModifiers();
            if(Modifier.isStatic(mod) || Modifier.isFinal(mod)){
                continue;
            }
            Object mapValue = map.get(field.getName());
            if (mapValue == null) {
                continue;
            }
            if ( mapValue instanceof Map  ){
                mapValue = mapToObject((Map<Object, Object>) mapValue, field.getType());
            }
            /*if (field.getName().equals("questionContents")) {
				System.out.println();
			}*/
            field.setAccessible(true);

            if ( mapValue instanceof List<?>){
                List<Object> list = (List<Object>) mapValue;
                ParameterizedType pt = (ParameterizedType) field.getGenericType();
                for (int i = 0; i < list.size(); i++) {
                    list.set(i, mapToObject((Map<Object, Object>) list.get(i), (Class)pt.getActualTypeArguments()[0]));
                }
                field.set(tObj, list);
                continue;
            }
            if (field.getType().getName().equals(mapValue.getClass().getName())) {
                field.set(tObj, mapValue);
            }else {
                if(field.getType().getName().equals(Long.class.getName())) {
                    field.set(tObj, (long)(double)mapValue);
                    continue;
                }
                if(field.getType().getName().equals(Integer.class.getName())){
                    if(mapValue.getClass().getName().equals(String.class.getName())) {
                        field.set(tObj, Integer.parseInt((String) mapValue));
                    }else {
                        field.set(tObj, (int)(double)mapValue);
                    }
                    continue;
                }
                if (field.getType().getName().equals(BigDecimal.class.getName())) {
                    if(mapValue.getClass().getName().equals(String.class.getName())) {
                        field.set(tObj, BigDecimal.valueOf(Double.parseDouble((String) mapValue)));
                    }else {
                        field.set(tObj, BigDecimal.valueOf((double)mapValue));
                    }
                    continue;
                }
                if (field.getType().getName().equals(Double.class.getName())) {
                    if(mapValue.getClass().getName().equals(String.class.getName())) {
                        field.set(tObj, Double.parseDouble((String) mapValue));
                    }else if(mapValue.getClass().getName().equals(BigDecimal.class.getName())){
                        field.set(tObj, ((BigDecimal)mapValue).doubleValue());
                    }else {
                        field.set(tObj,(double)mapValue);
                    }
                    continue;
                }
            }
        }
        return tObj;
    }
}