package com.infinitus.activity.util;

import org.springframework.beans.factory.annotation.Value;

/**
 * @create: 2019/1/9
 * @desc:
 */
public class KeyGenerator {

    //同步中奖纪录队列
    private static final String ACTIVITY_LIST_WINNING_SYNCH = "activity:list:winning:synch:";

    //中奖信息备份
    private static final String ACTIVITY_LIST_WINNING_BACKUP = "activity:list:winning:backup:";

    //收货地址备份
    private static final String ACTIVITY_LIST_ADDRESS_BACKUP = "activity:list:address:backup:";

    //活动配置
    private static final String ACTIVITY_CONFIG = "activity:config:";

    //活动总抽奖次数
    private static final String ACTIVITY_LOTTERY_COUNTS = "activity:lottery:counts:";

    //有资格抽奖用户的已抽奖次数
    private static final String ACTIVITY_QUALI_LOTTERY_COUNTS = "activity:quali:lottery:counts:";

    //奖品信息（用于抽奖）
    private static final String AWARDS_CONFIG = "awards:config:";

    //獎品已中奖数量
    private static final String AWARDS_ALREADY_COUNT = "awards:already:count:";


    //前端配置
    private static final String WEB_CONFIG = "web:config:";

    //登录标识
    private static final String AULDRAW_LOGIN_ALREADY = "auldraw:login:already:";

    //无抽奖资格是否已抽过奖标识
    private static final String AULDRAW_LOTTERY_ALREADY ="auldraw:lottery:already:";

    @Value("${deploycolId}")
    private String deploycolId;

    public static String getAuldrawLotteryAlready(String deploycolId,String dealerNo) {
        return deploycolId+":"+AULDRAW_LOTTERY_ALREADY +dealerNo;
    }

    public static String getAwardsAlreadyCount(String deploycolId) {
        return deploycolId + ":" +AWARDS_ALREADY_COUNT;
    }

    /**
     *
     * @param dealerNo 登录标识
     * @return
     */
    public static String getLoginAlready(String deploycolId , String dealerNo) {
        return deploycolId + ":" + AULDRAW_LOGIN_ALREADY + dealerNo;
    }

    /**
     * @Desc 前端配置
     */
    public static final String getWebConfig(String deploycolId) {
        return deploycolId+":"+WEB_CONFIG;
    }

    /**
     * 同步中奖纪录队列
     *
     * @return
     */
    public static String getActivityListWinningSynch(String deploycolId , String dealerNo) {
        return deploycolId+":"+ACTIVITY_LIST_WINNING_SYNCH + dealerNo;
    }

    /**
     * 获取中奖数据备份
     *
     * @return
     */
    public static String getActivityListWinningBackup(String deploycolId , String dealerNo) {
        return deploycolId+":"+ACTIVITY_LIST_WINNING_BACKUP+dealerNo;
    }

    /**
     * 获取收货地址备份
     *
     * @return
     */
    public static String getAwardsAddressWinningBackup(String deploycolId,String dealerNo) {
        return deploycolId+":"+ACTIVITY_LIST_ADDRESS_BACKUP +dealerNo;
    }

    /**
     * 获取活动设置
     *
     * @return
     */
    public static String getActivityConfig(String deploycolId) {
        return deploycolId+":"+ACTIVITY_CONFIG;
    }


    /**
     * 获取奖品设置（用于抽奖）
     *
     * @return
     */
    public static String getAwardsConfig(String deploycolId) {
        return deploycolId+":"+AWARDS_CONFIG;
    }

    /**
     * 获取活动的总抽奖次数
     *
     * @return
     */
    public static String getActivityLotteryCounts(String deploycolId) {
        return deploycolId+":"+ACTIVITY_LOTTERY_COUNTS;
    }

    /**
     * 有资格抽奖用户的已抽奖次数
     *
     * @return
     */
    public static String getActivityQualiLotteryCounts(String deploycolId, String dealerNo) {
        return deploycolId+":"+ACTIVITY_QUALI_LOTTERY_COUNTS + dealerNo;
    }

}