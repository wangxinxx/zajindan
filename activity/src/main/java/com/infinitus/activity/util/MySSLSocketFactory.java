package com.infinitus.activity.util;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.http.conn.ssl.SSLSocketFactory;

/**
 * @create: 2019/1/9
 * @desc:
 */
public class MySSLSocketFactory extends SSLSocketFactory {
    static{
        mySSLSocketFactory = new MySSLSocketFactory(createSContext());
    }

    protected static MySSLSocketFactory mySSLSocketFactory = null;



    protected static SSLContext createSContext(){
        SSLContext sslcontext = null;
        try {
//			sslcontext = SSLContext.getInstance("SSL");
            sslcontext = SSLContext.getInstance(MySSLSocketFactory.TLS);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            sslcontext.init(null, new TrustManager[]{new TrustAnyTrustManager()}, null);
        } catch (KeyManagementException e) {
            e.printStackTrace();
            return null;
        }
        return sslcontext;
    }

    protected MySSLSocketFactory(SSLContext sslContext) {
        super(sslContext);
        this.setHostnameVerifier(ALLOW_ALL_HOSTNAME_VERIFIER);
    }

    protected static MySSLSocketFactory getInstance(){
        if(mySSLSocketFactory != null){
            return mySSLSocketFactory;
        }else{
            return mySSLSocketFactory = new MySSLSocketFactory(createSContext());
        }
    }
}