package com.infinitus.activity.util;

import java.io.*;

/**
 * @create: 2019/1/9
 * @desc: 文件工具类
 */
public class FileUtils {
    /**
     *
     * @Title: createFileFolder
     * @Description: 创建多级文件夹
     * @author    add   by zhangxh 2016年12月26日 上午9:18:55
     * @param fileDirPath
     * @throws
     *
     */
    public static void createFileFolder(String fileDirPath){
        File pageElementFileDir = new File(fileDirPath);
        if (!pageElementFileDir.exists()) {
            pageElementFileDir.mkdirs();
        }
    }
    /**
     *
     * @Title: writeFile
     * @Description:  根据文件内容生产文件
     * @author    add   by zhangxh 2016年12月23日 下午4:35:56
     * @param content
     * @param fileName
     * @return
     * @throws Exception
     * @throws
     *
     */
    public static boolean writeFile(String content, File fileName,boolean isAdd) throws Exception {
        RandomAccessFile mm = null;
        boolean flag = false;
        FileOutputStream o = null;
        try {
            if(!fileName.exists()){
                fileName.createNewFile();
            }
            o = new FileOutputStream(fileName,isAdd);
            o.write(content.getBytes("UTF-8"));
            o.close();
            flag = true;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        } finally {
            if (mm != null) {
                mm.close();
            }
        }
        return flag;
    }
    /**
     *
     * @Title: clearFiles
     * @Description: 删除文件夹和文件夹以下的文件
     * @author    add   by zhangxh 2016年12月21日 上午10:24:32
     * @param workspaceRootPath
     * @throws
     *
     */
    public static void clearFiles(String workspaceRootPath){
        File file = new File(workspaceRootPath);
        if(file.exists()){
            deleteFile(file);
        }
    }
    /**
     *
     * @Title: deleteFile
     * @Description:递归删除文件
     * @author    add   by zhangxh 2016年12月21日 上午10:25:10
     * @param file
     * @throws
     *
     */
    public static  void deleteFile(File file){
        if(file.isDirectory()){
            File[] files = file.listFiles();
            for(int i=0; i<files.length; i++){
                deleteFile(files[i]);
            }
        }
        file.delete();
    }

    /**
     *
     * @Title: readContent
     * @Description: 读取txt文件的内容
     * @author    add   by zhangxh 2017年3月8日 下午2:13:43
     * @param file
     * @return
     * @throws
     *
     */
    public static StringBuilder readContent(File file){
        StringBuilder result = new StringBuilder();
        try{
            if( file.exists() ){
                BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
                String s = null;
                while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                    //result.append(System.lineSeparator()+s);
                    result.append(s);
                }
                br.close();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return result;
    }
}