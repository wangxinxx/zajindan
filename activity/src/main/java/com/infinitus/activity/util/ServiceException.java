package com.infinitus.activity.util;

import org.apache.commons.lang3.StringUtils;
/**
 * @create: 2019/1/9
 * @desc:
 */
public class ServiceException extends Exception{
    private static final long serialVersionUID = 650574801953629264L;

    private String errorMsg;

    public ServiceException(String message) {
        super(message);
    }


    public ServiceException(int httpCode) {
        switch(httpCode){
            case 400:
                errorMsg = "请求语法错误";
                break;
            case 401:
                errorMsg = "要求用户的身份认证";
                break;
            case 403:
                errorMsg = "拒绝执行此请求";
                break;
            case 404:
                errorMsg = "请求未找到";
                break;
            case 405:
                errorMsg = "方法被禁止";
                break;
            case 408:
                errorMsg = "请求超时";
                break;
            case 500:
                errorMsg = "服务器内部错误";
                break;
            case 501:
                errorMsg = "服务器不支持请求的功能";
                break;
            case 502:
                errorMsg = "502无效的请求";
                break;
            case 503:
                errorMsg = "超载或系统维护";
                break;
            case 505:
                errorMsg = "服务器不支持请求的HTTP协议的版本";
                break;
            default:
                errorMsg = httpCode + "其他http异常";
                break;
        }
    }

    @Override
    public String getMessage() {
        // TODO Auto-generated method stub
        if(StringUtils.isEmpty(errorMsg)){
            return super.getMessage();
        }
        return errorMsg;
    }
}