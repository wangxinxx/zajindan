package com.infinitus.activity.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @create: 2019/1/9
 * @desc: 参数断言类, 主要功能是为了判断参数是否符合业务逻辑
 */
public class ParamAssert {
    /**
     * 判断是否为手机号
     * 		如果是, 则返回true;反之,返回false
     * 		如果为null,则抛出IllegalArgumentException异常.
     * @param phone
     * @return
     */
    public static boolean isPhone(String phone) {
        // 如果手机号为空, 则跑出异常
        isNull(phone, "手机号不能为null!");
        // 手机号正则表达式
        String regExp = "^1\\d{10}$";

        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(phone);

        return matcher.matches();
    }

    /**
     * 判断对象是否为null
     * 		如果为null,则抛出IllegalArgumentException异常, 异常信息可以指定
     * @param object 需要判断的对象
     * @param message 自定义异常信息
     */
    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * 判断对象是否为null
     * 		如果为null, 返回true
     * 		如果非null, 返回false
     * @param object
     * @return boolean
     */
    public static boolean isNull(Object object) {
        return object == null;
    }

    /**
     * 判断对象是否为null或者""字符串
     * 		如果为是, 返回true
     * 		反之, 返回false
     * @param str
     * @return boolean
     */
    public static boolean isBlank(String str) {
        return str == null || "".equals(str);
    }

    /**
     * 判断受检测对象是否是指定的类型
     * 		如果指定类型为null, 则抛出IllegalArgumentException异常;
     * @param type 检测类型
     * @param object 受检测对象
     * @return boolean
     */
    public static boolean isInstanceOf(Class<?> type, Object object) {
        isNull(type, "Type to check against must not be null");
        return type.isInstance(object);
    }
}