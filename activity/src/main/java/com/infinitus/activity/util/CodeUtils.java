package com.infinitus.activity.util;

import java.util.UUID;

/**
 * @author: luzhenjie
 * @create: 2019/3/15
 * @desc:
 */
public class CodeUtils {

    /**
     * @Description:getUUID 获取去除“-”的UUID作为数据库中的code
     * @Param:
     * @Return:java.lang.String
     * @Author:Lin
     */
    public static String getUUID(){
        String code = UUID.randomUUID().toString();
        return code.replace("-","");
    }
}