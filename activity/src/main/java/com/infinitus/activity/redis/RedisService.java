package com.infinitus.activity.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @create: 2019/1/9
 * @desc:
 */
@Service
public class RedisService {
    private static final Logger logger = LoggerFactory.getLogger(RedisService.class);

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 删除指定key的value
     * @param key
     */
    public void del(String key){
        /*Jedis jedis=null;
        jedis=getJedis();*/
        stringRedisTemplate.delete(key);

    }

    /**
     * 保存指定key值的value
     * @param key
     * @param value
     */
    public void set(String key, String value){

        stringRedisTemplate.opsForValue().set(key, value);
    }
    /**
     * 得到指定key值的value
     * @param key
     */
    public Object get(String key){
       return stringRedisTemplate .opsForValue().get(key);
    }

}