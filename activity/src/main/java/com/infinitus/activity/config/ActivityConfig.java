package com.infinitus.activity.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.infinitus.activity.pojo.VO.ActivityConfigVO;
import com.infinitus.activity.pojo.VO.AwardsConfigVO;
import com.infinitus.activity.util.KeyGenerator;
import com.infinitus.activity.util.MapToObjectConvertionUtil;
import com.infinitus.activity.util.StringUtils;
import org.hibernate.validator.HibernateValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author: luzhenjie
 * @create: 2019/3/5
 * @desc:
 */
@Configuration
public class ActivityConfig {

    private final static String CONFIG_TYPE_PRIZZE="prize";
    private final static String CONFIG_TYPE_BASE="base";
    private final static String CONFIGTYPE ="configtype";
    private final static String GROUPLIST = "groupList";
    private final static String GROUP_ID  = "group_id";

    @Value("${deploycolId}")
    private String deploycolId;

    @Value("${spring.redis.host}")
    private String redisHost;

    @Value("${spring.redis.port}")
    private String redisPort;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Bean
    public ActivityConfigVO activityConfigVO(){
        Jedis jedis = null;
        try {
            String activityConfigJson= stringRedisTemplate.opsForValue().get(KeyGenerator.getActivityConfig(deploycolId));
            if(StringUtils.isEmpty(activityConfigJson)){
                return new ActivityConfigVO();
            }
            JSONObject activityConfigJsonObject = JSON.parseObject(activityConfigJson);
            JSONArray obj = (JSONArray) activityConfigJsonObject.get(GROUPLIST);
            ActivityConfigVO activityConfigVO =null;
            List<AwardsConfigVO> awardsConfigVOList = new ArrayList<AwardsConfigVO>();
            for (Object object : obj) {
                JSONObject group = (JSONObject) object;
                String groupId = (String) group.get(GROUP_ID);
                JSONArray jsonarray = (JSONArray) activityConfigJsonObject.get(groupId);
                Map<Object, Object> mapTemp = MapToObjectConvertionUtil.josnArrayToMap(jsonarray);
                if(null!=mapTemp.get(CONFIGTYPE)&&mapTemp.get(CONFIGTYPE).equals(CONFIG_TYPE_PRIZZE)) {
                    AwardsConfigVO awardsConfigVO = MapToObjectConvertionUtil.mapToObject(mapTemp, AwardsConfigVO.class);
                    awardsConfigVOList.add(awardsConfigVO);
                }else if(null!=mapTemp.get(CONFIGTYPE)&&mapTemp.get(CONFIGTYPE).equals(CONFIG_TYPE_BASE)) {
                    activityConfigVO = MapToObjectConvertionUtil.mapToObject(mapTemp, ActivityConfigVO.class);
                }
            }
            if(activityConfigVO!=null&&awardsConfigVOList.size()>0) {
                activityConfigVO.setAwardsConfigList(awardsConfigVOList);
            }

            String activity1 = stringRedisTemplate.opsForValue().get(KeyGenerator.getAwardsConfig(deploycolId));
            if(activity1 == null) {
                //将奖品信息写入Redis中
                JSONArray jsonArray = new JSONArray();
                List<AwardsConfigVO> list = activityConfigVO.getAwardsConfigList();
                for (AwardsConfigVO awardsConfigVO : list) {
                    jsonArray.add(awardsConfigVO);
                }
                stringRedisTemplate.opsForValue().set(KeyGenerator.getAwardsConfig(deploycolId),jsonArray.toJSONString());
            }
            jedis = new Jedis(redisHost, Integer.valueOf(redisPort));
            jedis.connect();
            Boolean exists = jedis.exists(KeyGenerator.getAwardsAlreadyCount(deploycolId));
            if(!exists) {
                for (AwardsConfigVO awardsConfigVO : awardsConfigVOList) {
                    jedis.hset(KeyGenerator.getAwardsAlreadyCount(deploycolId),awardsConfigVO.getAwardsCode(),"0");
                }
            }
            return activityConfigVO;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(jedis != null) {
                jedis.close();
            }
        }
        return null;
    }


}