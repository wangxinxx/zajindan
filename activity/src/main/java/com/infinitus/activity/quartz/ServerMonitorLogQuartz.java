package com.infinitus.activity.quartz;

import com.infinitus.activity.util.FileUtils;
import com.infinitus.activity.util.SigarUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;
import java.util.Calendar;

/**
 * @create: 2019/1/9
 * @desc:
 */

@Lazy(value = false)
public class ServerMonitorLogQuartz {
    public void execute(){
        writeTxt();
    }

    public static void main(String args[]){
        writeTxt();
    }
    /**
     *
     * @Title: writeTxt
     * @Description: 写日志文件
     * @author    add   by zhangxh 2017年3月1日 上午9:44:49
     * @throws
     *
     */
    @Scheduled(cron = "0/5 * * * * ?")
    static void writeTxt() {
        try {
//            StringBuilder info = new StringBuilder();
//            Sigar sigar = SigarUtils.getInstance();
            //CpuInfo infos[] = sigar.getCpuInfoList();
            //info.append("\"countCpu\":").append(infos.length);
            Calendar c = Calendar.getInstance();// 可以对每个时间域单独修改
            int date = c.get(Calendar.DATE);
            String timeTxt = c.get(Calendar.HOUR)+"-"+c.get(Calendar.MINUTE)+"-"+c.get(Calendar.SECOND);
            String result =  timeTxt + "|"+ SigarUtils.cpu()+SigarUtils.memory()+SigarUtils.fileIo()+SigarUtils.netIo()+";";

//			 String path = Paths.get(PathKit.getWebRootPath(),"logstxt").toString();
            //System.out.println(path);
            // 本地调试用
            String path = "/Users/icuicy/Documents/code/infinitus/nestwebtemplate/src/main/webapp/logstxt/";
            // 删除前一天的
            c.add(Calendar.DATE, -1);// 日期减1
            // 删除前一天的
            int lastDate = c.get(Calendar.DATE);
            File fileLast = new File(path + "ecs" + lastDate + ".txt");
            FileUtils.deleteFile(fileLast);
            //明细文件
            String fileName = "ecs" + date + ".txt";
            FileUtils.createFileFolder(path);
            File file = new File(path +File.separator+ fileName);

            StringBuilder content = FileUtils.readContent(file);
            String fileContent = "";
            if( content.length()>0 ){
                content = content.insert(0, result);
                String arr[] = content.toString().split(";");
                if( arr.length == 100 ){
                    for( int i=0;i<arr.length-1;i++ ){
                        fileContent += arr[i]+";";
                    }
                }else{
                    fileContent = content.toString();
                }

            }else{
                content = content.insert(0, result);
                fileContent = content.toString();
            }
            FileUtils.writeFile(fileContent, file, false);
            //System.out.println(fileContent);
            //文件最新记录
            String fileNameNew = "ecs.txt";
            FileUtils.createFileFolder(path);
            File fileNew = new File(path +File.separator+ fileNameNew);
            FileUtils.writeFile(result, fileNew, false);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}