package com.infinitus.activity.quartz;

import com.infinitus.activity.service.PushListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @create: 2019/1/9
 * @desc:
 */
//@Component("scheduling")
public class pushListQuartz {
    @Autowired
    PushListService pushListService;

    //@Scheduled(cron = "*/3 * * * * ?")
    public void pushWinningData() {
//		pushListService.pushWinningData();
    }

//    @Scheduled(cron = "0 */2 * * * ?")
    public void sendWinningDataToDB() {
        pushListService.sendWinningDataToDB();
    }
}