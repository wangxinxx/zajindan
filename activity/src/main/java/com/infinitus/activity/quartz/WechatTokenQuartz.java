package com.infinitus.activity.quartz;

import com.infinitus.activity.common.RedisKey;
import com.infinitus.activity.redis.RedisService;
import com.infinitus.activity.util.HttpClientUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @create: 2019/1/9
 * @desc:
 */
@Lazy(value = false)
@Component
public class WechatTokenQuartz {
    private static Logger logger = Logger.getLogger(WechatTokenQuartz.class);

    @Autowired
    RedisService redisService;
    @Value("${wechat.env}")
    String wechatEnv;
    /**
     *
     * @Title: writeTxt
     * @Description: 写日志文件
     * @author    add   by zhangxh 2017年3月1日 上午9:44:49
     * @throws
     *
     */
//	@Scheduled(cron = "0/60 * * * * *")
    private void writeTxt() {
        try {
            logger.error("redis更新JsTicket-----------------------------");
            String jsTicket= HttpClientUtils.doGet(wechatEnv+"wechat-back/redis/getKey?keyName=JSAPI_TICKET",null);
            if(jsTicket!=null&&!jsTicket.isEmpty()){
                redisService.set(RedisKey.WECHAT_jsTicker,jsTicket);
            }
            String accessToken=HttpClientUtils.doGet(wechatEnv+"wechat-back/redis/getKey?keyName=ACCESS_TOKEN",null);
            if(accessToken!=null&&!accessToken.isEmpty()){
                redisService.set(RedisKey.WECHAT_accessToken,accessToken);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}