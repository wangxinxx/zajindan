package com.infinitus.activity;

import com.infinitus.activity.util.AwardsUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author: luzhenjie
 * @create: 2019/1/16
 * @desc:
 */
public class TestRandom {

    private static ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();

    @Test
    public void test() {
    }

    @Test
    public void test2() {

        String str = "123456,";

        String substring = str.substring(0, str.length() - 1);
        System.out.println(substring);
    }

    @Test
    public void testAwardsUtil() {

        List<String> awardsList = new LinkedList<>();
        awardsList.add("iphonex");
        awardsList.add("macpro");
        awardsList.add("ipad");

        List<Double> rateList = new LinkedList<>();
        rateList.add(0.623);
        rateList.add(0.0087);
        rateList.add(0.3683);

        int total = 100098;

        double award1 = 0;
        double award2 = 0;
        double award3 = 0;

        for (int i = 0; i < total; i++) {
            int awardsIndex = AwardsUtils.drawGift(rateList);
            String awards = awardsList.get(awardsIndex);
            switch (awards) {
                case "iphonex":
                    award1+=1;
                    break;
                case "macpro":
                    award2+=1;
                    break;
                case "ipad":
                    award3+=1;
                    break;
            }
        }
        System.out.println("iphonex抽中次数：" + award1+",概率为："+String.format("%.4f", award1/new Double(total)));
        System.out.println("macpro抽中次数：" + award2+",概率为："+String.format("%.4f", award2/new Double(total)));
        System.out.println("ipad抽中次数：" + award3+",概率为："+String.format("%.4f", award3/new Double(total)));
    }
}