// 通用 登录获取记录 // 依赖layer
var LoginVo = {
    getVo: function () {
        return localStorage.getItem("isLogin") ? JSON.parse(localStorage.getItem("isLogin")) : {
            login: 0,
            staffNum: ""
        }
    },
    setVo: function (obj) {
        localStorage.setItem("isLogin", JSON.stringify(obj))
    },
    getStaffNum: function () {
        return this.getVo().staffNum
    },
    getLogin: function () {
        return this.getVo().login
    },
    getIsLogin: function () {
        return this.getLogin() == 1
    },



}
// 行为
var LoginActionVo = {
    openLogin: function (type) {
        layer.open({
            type: 3,
            title: [
                '登录您的账号',
                'background-color:transparent; color:black; text-align:center;font-size:22px'
            ],
            className: 'login-popuo',
            content: '<div class="pwd" style="height:32px;line-height: 32px;"><input style="height:32px;line-height: 32px;font-size:16px" class="staffNum" name="staffNum" type="tel" placeholder="请输入您的卡号" /></div><div class="pwd" style="height:32px;line-height: 32px;"><input style="height:32px;font-size:16px" class="password" name="password" type="password" placeholder="请输入您的密码" /></div><p class="error" style="font-size:16px;margin-bottom:18px"></p><div style="text-align:center;width:100%;height:86px;background:#fff;position:absolute;bottom:0px;left: 0px;padding: 0 14px;box-sizing:border-box;border-radius: 5px;"><div style="font-size:16px;height:32px;line-height: 32px;" class="button" data-type=' + type + '>登录</div><div class="button" style="font-size:16px;height:32px;line-height: 32px;margin-top:10px">关闭</div></div>',
            style: 'width:calc(100vw - 60px);max-width:350px;min-height:300px; background-color:#fff; color:black; border:none;border-radius:6px;box-shadow: 0px 0px 15px #000000;font-size:18px',
            shadeClose: false
        });

    }
}

// 地区 通用
var AddressVo = {
    getCity: function (data) {
        var a = b = c = '';
        $.each(window.cityData, function (i, province) {
            if (province.code === data.provinceCode) {
                a = province.name + ' ';
                $.each(province.sub, function (i, city) {
                    if (city.code === data.cityCode) {
                        b = city.name + ' ';
                        $.each(city.sub, function (i, town) {
                            if (town.code === data.townCode) {
                                c = town.name + ' ';
                            }
                        })
                    }
                })
            }
        })
        return a + b + c;
    }
}

// 抽奖 通用
var LotteryVo = {
    common:{},
    modalCommonStyle: 'width:calc(100vw - 60px);max-width:350px; background-color:#fff; color:black; border:none;border-radius:6px;box-shadow: 0px 0px 15px #000000;font-size:18px',
    modalButtonStyle: 'font-size:16px;height:32px;line-height: 32px;margin-top:0;',
    modalButtonContStyle: 'text-align:center;width:100%;height:40px;background:#fff;position:absolute;bottom:0px;left: 0px;padding: 0 14px;box-sizing:border-box;border-radius: 5px;',
    // 获取 剩余抽奖资格
    getLotteryCount: function () {
        if (LoginVo.getLogin() !== 0) {
            $.ajax({
                type: 'get',
                url: this.common.url + "/auldraw/quali/count?dealerNo=" + LoginVo.getStaffNum(),
                success: function (data) {
                    $("#lotteryTimes").html("您还有" + data.content.count + "次抽奖机会");
                    window.restCount = data.content.count
                },
            });
        }
    },
    // egg 记录对应弹框

    // 抽奖次数为 0 
    handleLotteryNoOfZero: function () {
        
        layer.open({
            type: 3,
            title: [
                '温馨提示',
                'background-color:transparent; color: black; text-align:center;'
            ],
            className: 'startNotice',
            content: '<div class="zjnotice">' + '您抽奖已超过指定次数' +
            '</div><div style="'+ this.modalButtonContStyle +'"><div class="button" style="'+ this.modalButtonStyle +'">' + ('关闭') + '</div></div>' + (this.common.skipUrl ? '<div class="button">' + '<a style="text-decoration: none; color: #ff8800;" href="' + this.common.skipUrl + '">返回</a>' + '</div>' : ''),
            style: this.modalCommonStyle,
            shadeClose: false,
            end: function () {
                // location.href = common.skipUrl;
            }
        });
        $(".startNotice  .button").on("click", function () {
            layer.closeAll();
            LotteryVo.getLotteryCount();
        })
        $(".startNotice #checkHistory").on("click", function () {
            $('.btn').click();
        })
    },
    // 抽奖未开始
    handleLotteryUnStart: function () {
        layer.open({
            type: 3,
            title: '温馨提示',
            className: 'againNotice',
            content: '<div class="notice">' + 
                    '抽奖活动尚未开始，敬请期待！' +
                '</div><div style="'+ this.modalButtonContStyle +'"><div style="'+ this.modalButtonStyle +'" class="button">确定</div></div>',
            style: this.modalCommonStyle,
            shadeClose: false,
            end: function () {
                click = false;
            }
        });
        $(".againNotice  .button").on("click", function () {
            layer.closeAll();
            LotteryVo.getLotteryCount();
        })
    },
    // 抽奖已结束
    handleLotteryHasBeenEnd: function () {
        layer.open({
            type: 3,
            title: '温馨提示',
            className: 'againNotice',
            content: '<div class="notice">' + 
                    '抽奖活动已结束，感谢关注！' +
                '</div><div style="'+ this.modalButtonContStyle +'"><div style="'+ this.modalButtonStyle +'" class="button">确定</div></div>',
            style: this.modalCommonStyle,
            shadeClose: false,
            end: function () {
                click = false;
            }
        });
        $(".againNotice  .button").on("click", function () {
            layer.closeAll();
            LotteryVo.getLotteryCount();
        })
    },
    // 抽奖用户未登录
    handleLotteryUnLogin: function () {
        layer.open({
            type: 3,
            title: '温馨提示',
            className: 'errortNotice',
            content: '<div class="notice">卡号未登录，请稍后再试！</div><div style="'+ this.modalButtonContStyle +'"><div style="'+ this.modalButtonStyle +'" class="button">确定</div></div>',
            style: this.modalCommonStyle,
            shadeClose: false,
        });
        $(".errortNotice ").on("click", '.button', function () {
            location.reload();
        })
    },

    // 超过次数
    handleLotteryOverTimes: function () {
        layer.open({
            type: 3,
            title: [
                '温馨提示',
                'background-color:transparent; color:black; text-align:center;'
            ],
            className: 'startNotice',
            content: '<div class="zjnotice">' + '您抽奖已超过指定次数' +
                '<div style="'+ this.modalButtonContStyle +'"><div style="'+ this.modalButtonStyle +'" class="button">' + (
                    '<a style="text-decoration: none; color: #ff8800;" href="javascript:void(0);" id="checkHistory">查看中奖记录</a>'
                ) + '</div></div>',
            style: this.modalCommonStyle,
            shadeClose: false,
            end: function () {
                // location.href = common.skipUrl;
            }
        });
        $(".startNotice").on("click", '.button',function () {
            layer.closeAll();
            LotteryVo.getLotteryCount();
        })

        $(".startNotice").on("click", '#checkHistory',function () {
            $('.btn').click();
        })
    },

    // 没有抽奖资格
    handleLotteryUnqualified: function () {
        layer.open({
            type: 3,
            title: [
                '温馨提示',
                'background-color:transparent; color:black; text-align:center;'
            ],
            className: 'startNotice',
            content: '<div class="zjnotice">你已没有抽奖资格</div><div style="'+ this.modalButtonContStyle +'"><div style="'+ this.modalButtonStyle +'" class="button">关闭</div></div>' + (this.common.skipUrl ? '<div class="button">' + '<a style="text-decoration: none; color: #ff8800;" href="' + this.common.skipUrl  + '">返回</a>' + '</div>' : ''),
            style: this.modalCommonStyle,
            shadeClose: false,
            end: function () {
                // location.href = common.skipUrl;
            }
        });
        $(".startNotice ").on("click", '.button', function () {
            layer.closeAll();
            LotteryVo.getLotteryCount();
        })
    },

    // 抽奖不中奖
    handleLotteryNoPrizes: function () {
        console.log(this.modalButtonStyle,this)
        layer.open({
            type: 3,
            title: [
                '温馨提示',
                'background-color:transparent; color:black; text-align:center;'
            ],
            className: 'startNotice',
            content: '<div class="zjnotice">未中奖</div><div style="'+ this.modalButtonContStyle +'"><div style="'+ this.modalButtonStyle +'" class="button">关闭</div></div>' + (this.common.skipUrl ? '<div class="button">' + '<a style="text-decoration: none; color: #ff8800;" href="' + this.common.skipUrl  + '">返回</a>' + '</div>' : ''),
            style: this.modalCommonStyle,
            shadeClose: false,
            end: function () {
                
            }
        });
        $(".startNotice").on("click",'.button', function () {
            layer.closeAll();
            LotteryVo.getLotteryCount();
        })
    },

    // 抽奖 中奖提示
    handleLotteryPrizes: function (msg,data) {
        var awardsCode = data.content.awardsCode
        var status = this.common["prize"][awardsCode].status
        var isType = status == '1' // 实物
        var qualiSeqNo = localStorage.getItem("qualiSeqNo")
        layer.open({
            title: '温馨提示',
            className: 'drawNotice',
            // content: '<div class="zjnotice">' + msg + '</div><div class="button">' + (common["prize"][data.content.awardsCode].status == "1" ? '<a class="addressModal" style="color: inherit;display:inline;margin-left:10px"' + 'data-quali="' + localStorage.getItem("qualiSeqNo") + '"' + 'class="addressModal" href="javascript:void(0);' + '">' + '点此填写收货地址</a>':'关闭') + '</div>',
            content: '<div class="zjnotice">' + msg + '</div>' + ( isType 
                        ? '<div style="'+ this.modalButtonContStyle +'"><div class="address_modal" style="'+ this.modalButtonStyle +'" ><a class="addressModal" style="color: inherit;display:inline;margin-left:10px"' + 'data-quali="' + qualiSeqNo + '"' + 'class="addressModal" href="javascript:void(0);' + '">' + '点此填写收货地址</a>' + '</div></div>' 
                        : '<div style="'+ this.modalButtonContStyle +'"><div style="'+ this.modalButtonStyle +'" class="button">' +'关闭' + '</div></div>'),
            style: this.modalCommonStyle,
            shadeClose: false,
            end: function () {
            }
        });
        $(".drawNotice ").on("click", '.button', function () {
            layer.closeAll();
            LotteryVo.getLotteryCount();
        })
    },

    // 中奖记录
    handleOpenLotteryRecord: function (str) {
        layer.open({
            title: '中奖记录',
            className: 'drawNotice',
            content: str + '<div style="'+ this.modalButtonContStyle +'"><div style="'+ this.modalButtonStyle +'" class="button">确定</div></div>',
            style: this.modalCommonStyle,
            shadeClose: false,
            end: function () {
            }
        });
        $(".drawNotice").on("click", '.button', function () {
            layer.closeAll();
            LotteryVo.getLotteryCount();
        })
    },
    
    // 中奖查看填写地址
    handleLotteryOfAddress: function (str) {
        layer.open({
            title: '查看收货地址',
            className: 'addressFormDetailForm',
            content: str + '<div style="'+ this.modalButtonContStyle +'">' + '<button class="button" id="cancel" style="'+ this.modalButtonStyle +' width:60px" >取消</button>' + '</div>',
            style: this.modalCommonStyle,
            shadeClose: false,

        });
        $(".addressFormDetailForm ").on("click", '#cancel',function () {
            layer.closeAll();
            LotteryVo.getLotteryCount();
        });
    },

    // 中奖查看填写地址
    handleLotteryFillAddress: function (str) {
        layer.open({
            title: '填写收货地址',
            className: 'addressFormModal',
            content: str + '<div style="'+ this.modalButtonContStyle +'">' + '<button class="button" id="submitAddress" style="'+ this.modalButtonStyle +'width:60px;margin-right:10px;">提交</button>' + '<button class="button" id="cancel" style="'+ this.modalButtonStyle +'width:60px;">取消</button>' + '</div>',
            style: this.modalCommonStyle,
            shadeClose: false,
            scrollbar: false,
            end: function () {
                // if (!(lottery.data.status == 1)) {
                //     location.href = common.skipUrl;
                // }
            }
        });

        $(".addressFormModal").on("click", "#cancel" ,function () {
            layer.closeAll();
            LotteryVo.getLotteryCount();
        });
    },

    // 提交地址 错误提示
    handleSubmitAddressErrorTip: function (msg) {
        layer.open({
            type: 3,
            title: '温馨提示',
            className: 'errortNotice',
            content: '<div class="notice">' + msg + '</div><div style="'+ this.modalButtonContStyle +'"><div style="'+ this.modalButtonStyle +' " class="button">确定</div></div>',
            style: this.modalCommonStyle,
            shadeClose: false,
        });

        $(".errortNotice .button").on("click", function () {
            layer.close($(".errortNotice .button").closest(".layermbox").attr("index"));
        })
    },
    // 提交地址 成功提示
    handleSubmitAddressSuccessTip: function (msg) {
        layer.open({
            title: '温馨提示',
            className: 'drawNotice',
            content: '<div class="zjnotice t-c">' + msg +
                '</div><div style="'+ this.modalButtonContStyle +'"><div style="'+ this.modalButtonStyle +'margin:0" class="button">' + '关闭' + '</div></div>' + (this.common.skipUrl ? '<div class="button">' + '<a style="text-decoration: none; color: #ff8800;" href="' + this.common.skipUrl + '">返回</a>' + '</div>' : ''),
            style: this.modalCommonStyle,
            shadeClose: false,
            end: function () {
                // location.href = common.skipUrl;
            }
        });
        $(".drawNotice  .button").on("click", function () {
            layer.closeAll();
            LotteryVo.getLotteryCount();
        })
    },
    // 已抽过奖
    handleOverLottery: function () {
        layer.open({
            type: 3,
            title: [
                '温馨提示',
                // 'background-color:#58a0ea; color:#fff; text-align:center;'
                'background-color: transparent; color:black; text-align:center;'
            ],
            className: 'againNotice',
            content: '<div class="zjnotice">您已经抽过奖了，不能重复抽奖，谢谢！</div><div style="'+ this.modalButtonContStyle +'"><div style="'+ this.modalButtonStyle +' " class="button">确定</div></div>',
            style: this.modalCommonStyle,
            shadeClose: false,
            end: function () {
                // location.href = common.skipUrl;
            }
        });
    }

}

// egg
var EggVo = {
    animation: function(dom) { // 触发中奖动画效果
        var opt = this.getTargetOption(dom)
        opt.el.addClass(opt._tAddClass);
        $('.lottery-text').addClass(opt._textClass);
        $('.egg-broken-wrap').addClass(opt._wrapClass);
        $('.hammer').addClass(opt._hammer);
        $('.light').addClass(opt._light);
        eggindex = opt.index;
    },
    getTargetOption : function(dom) { // 对应的蛋的动画和配置
        var $el = $(dom);
        var align = $el.attr('data-egg')
        var index = $el.index()
        var map = {
            el:$el,
            index:index,
            align:align,
        }
        if(align == 'left') {
            map._tAddClass = 'eggLeftClicked'
            map._textClass = 'showEggOpenAnimate'
            map._wrapClass = 'eggLeft showEggOpenAnimate'
            map._hammer = 'hammer-left'
            map._light = 'light-eggLeft showEggOpenAnimate'
        }
        if(align == 'middle') {
            map._tAddClass = 'eggMiddleClicked'
            map._textClass = 'showEggOpenAnimate'
            map._wrapClass = 'eggMiddle showEggOpenAnimate'
            map._hammer = 'hammer-middle'
            map._light = 'light-eggMiddle showEggOpenAnimate'
        }
        if(align == 'right') {
            map._tAddClass = 'eggRightClicked'
            map._textClass = 'showEggOpenAnimate'
            map._wrapClass = 'eggRight showEggOpenAnimate'
            map._hammer = 'hammer-right'
            map._light = 'light-eggRight showEggOpenAnimate'
        }    

        return map
    },
    init: function() {
        $('.egg-box').removeClass('eggLeftClicked eggMiddleClicked eggRightClicked')
        console.log('初始化',$('.egg-box'))
        $('.lottery-text').removeClass('showEggOpenAnimate');
        $('.egg-broken-wrap').removeClass('eggLeft eggMiddle eggRight showEggOpenAnimate');
        $('.hammer').removeClass('hammer-left hammer-middle hammer-right');
        $('.light').removeClass('light-eggLeft light-eggMiddle light-eggRight showEggOpenAnimate');
    }
 }

 // egg res code map

 var LotteryCodeMap = {
    40005:LotteryVo.handleLotteryUnStart.bind(LotteryVo),
    40006:LotteryVo.handleLotteryHasBeenEnd.bind(LotteryVo),
    40002:LotteryVo.handleLotteryUnLogin.bind(LotteryVo),
    40007:LotteryVo.handleLotteryOverTimes.bind(LotteryVo),
    40008:LotteryVo.handleLotteryUnqualified.bind(LotteryVo),
    40003:LotteryVo.handleLotteryNoPrizes.bind(LotteryVo),
 }

 // 通用关闭
 var layerVo = {
     end: function () {
        $('.layermcont').on('click','.button',function(){
            console.log('??重置')
            window.canDraw = true;
            console.log(canDraw,'??重置2')
        })
     }
 }