var locations = location.href;

var urls = "http://activity.infinitus.com.cn/nx_nx_0a3c15c1-984d-d5f15410";
    var urlPage = urls + "/static";
    // var urls = "http://10.108.1.174:8086";
    // var urlPage = '';
    // urls = "http://activity.infinitus.com.cn/nx_nx_bb7bf7dc-9311-37b38dda"

var common = {
    startTime : 1533691800000,          //获取开始时间
    endTime : 1543983132000,            //活动结束时间
    staffNum : "",                      //卡号
    url : urls,
    urlPage: urlPage,
    prize : {                           //奖品配置
        "1000" : {
            position : 0 ,      //奖品位置
            title: "享优乐净水器",    //奖品title
            status : 1          //奖品类别 1: 实物 2.体验资格 3：分享币 4： 未中奖
        },
        "1001" : {
            position: 1,
            title: "享优乐电动牙刷",
            status: 1
        },
        "1002" : {
            position: 5,
            title: "享优乐洁面仪",
            status: 1
        },
        "1003" : {
            position : 3 ,
            title: "享优乐成长枕",
            status : 1
        },
        "1004" : {
            position : 6 ,
            title: "纷享币1000个",
            status : 3
        },
        "1005" : {
            position: 2,
            title: "纷享币500个",
            status: 3
        },
        "1006" : {
            position: 4,
            title: "纷享币100个",
            status: 3
        },
        "8080" : {
            position : 7 ,
            title: "未中奖",
            status : 4
        }
    },
    skipUrl: "https://live.polyv.cn/watch/239952",
    notice1 : "目前参与人数较多，请稍后再试！"
}
