	var js_url = "uits.infinitus.com.cn/_bc/sugo-sdk-js/libs/sugoio-latest.min.js";
	
   (function(g,e){function m(b){b=c.atob(b);b=JSON.parse(b).state;c.sessionStorage.setItem("editorParams",JSON.stringify({accessToken:b.access_token,accessTokenExpiresAt:Date.now()+1E3*Number(b.expires_in),projectToken:b.token,projectId:b.project_id,userId:b.user_id,choosePage:b.choose_page}));b.hash?c.location.hash=b.hash:c.history?c.history.replaceState("",g.title,c.location.pathname+c.location.search):c.location.hash=""}if(!e.__SV){var c=window,d,k,l,h;c.sugoio=e;try{m(c.location.hash.replace("#",
      ""))}catch(b){}e._i=[];e.init=function(b,c,d){function g(b,c){var d=c.split(".");2===d.length&&(b=b[d[0]],c=d[1]);b[c]=function(){b.push([c].concat(Array.prototype.slice.call(arguments,0)))}}var f=e;"undefined"!==typeof d?f=e[d]=[]:d="sugoio";f.people=f.people||[];f.toString=function(b){var c="sugoio";"sugoio"!==d&&(c+="."+d);b||(c+=" (stub)");return c};f.people.toString=function(){return f.toString(1)+".people (stub)"};l="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
      for(h=0;h<l.length;h++)g(f,l[h]);e._i.push([b,c,d])};e.__SV=1.2;d=g.createElement("script");d.type="text/javascript";d.async=!0;"undefined"!==typeof SUGOIO_CUSTOM_LIB_URL?d.src=SUGOIO_CUSTOM_LIB_URL:d.src="file:"===c.location.protocol&&"//uits.infinitus.com.cn/_bc/sugo-sdk-js/libs/sugoio-latest.min.js".match(/^\/\//)?"https://uits.infinitus.com.cn/_bc/sugo-sdk-js/libs/sugoio-latest.min.js":"//uits.infinitus.com.cn/_bc/sugo-sdk-js/libs/sugoio-latest.min.js";k=g.getElementsByTagName("script")[0];k.parentNode.insertBefore(d,
      k)}})(document,window.sugoio||[]);
    
	(function(e,a){if(!a.__SV){var b=window;try{var c,n,k,l=b.location,g=l.hash;c=function(a,b){return(n=a.match(new RegExp(b+"=([^&]*)")))?n[1]:null};g&&c(g,"state")&&(k=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===k.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(k.desiredHash||"",e.title,l.pathname+l.search)))}catch(p){}var m,h;window.sugoio=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments, 0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="sugoio";d.people=d.people||[];d.toString=function(b){var a="sugoio";"sugoio"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};m="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");   for(h=0;h<m.length;h++)e(d,m[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;"undefined"!==typeof SUGOIO_CUSTOM_LIB_URL?b.src=SUGOIO_CUSTOM_LIB_URL:b.src="file:"===e.location.protocol&&("//"+js_url).match(/^\/\//)?("https://"+js_url):("//"+js_url);c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,
	c)}})(document,window.sugoio||[]);

	var sugoioConfig = {
		test: {
			appId: 'f0de3e8c1df242c71818c05cb911ef3b',
			projectId: 'com_rJFxdRfMg_project_Sk2bKrwbQBG',
		},
		produce: {
			appId: 'f0de3e8c1df242c71818c05cb911ef3b',
			projectId: 'com_rJFxdRfMg_project_Sk2bKrwbQBG',
		}
	};
	var sugoio_appId;
	var sugoio_projectId;
	if(location.host.indexOf('mdealer.infinitus') != -1){
		sugoio_appId = sugoioConfig.produce.appId;
		sugoio_projectId = sugoioConfig.produce.projectId;
	} else{
		sugoio_appId = sugoioConfig.test.appId;
		sugoio_projectId = sugoioConfig.test.projectId;
	}
	sugoio.init(sugoio_appId, { 		//appId
      	'project_id': sugoio_projectId, //sugoio_appId
      	loaded: function(lib) {
          	sugoio.time_event('停留');
			sugoio.register({
				"app_name"  : _app_name,
				"app_version"  :  _app_version ,
				"UserID"  : _user_id } );
          	sugoio._.register_event(window, 'beforeunload', function() {
              	sugoio.track('停留', {page: location.pathname})
          	}, false, true)
		}
    })